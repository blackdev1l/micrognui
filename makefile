ALL:
	go build -o out/apiService -i api/main.go
	go build -o out/ognui -i srv/main.go

proto:
	protoc proto/ognui.proto --go_out=plugins=micro:./

clean:
	- rm -rf out/
.PHONY: clean proto
