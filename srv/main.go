package main

import (
	proto "bitbucket.org/blackdev1l/micrognui/proto"
	"fmt"
	"github.com/micro/go-micro"
	context "golang.org/x/net/context"
	"gopkg.in/redis.v5"
	"io/ioutil"
	"log"
	"time"
)

type Ognui struct{}

func (o *Ognui) Save(ctx context.Context, req *proto.Request, rsp *proto.Response) error {
	log.Println("service called")
	client := redis.NewClient(&redis.Options{
		Addr:     "127.0.0.1:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	if client == nil {
		rsp.Result = "KO"
		return fmt.Errorf("redis not running")
	}

	sender := req.GetSender()
	receiver := req.GetReceiver()
	filename := req.GetFilename()
	file := req.GetFile()

	err := ioutil.WriteFile(filename, file, 0644)
	if err != nil {
		rsp.Result = "KO"
		return err
	}

	client.SAdd(receiver, filename)

	rsp.Result = fmt.Sprintf("file saved from %s to %s", sender, receiver)
	return nil
}

func (o *Ognui) List(ctx context.Context, req *proto.Request, rsp *proto.Response) error {
	client := redis.NewClient(&redis.Options{
		Addr:     "127.0.0.1:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	if client == nil {
		rsp.Result = "KO"
		return fmt.Errorf("redis not running")
	}

	receiver := req.GetReceiver()
	list := client.SMembers(receiver)
	rsp.Result = list.String()
	return nil
}

func (o *Ognui) Retrieve(ctx context.Context, req *proto.Request, rsp *proto.RetrieveRsp) error {
	client := redis.NewClient(&redis.Options{
		Addr:     "127.0.0.1:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	if client == nil {
		//rsp.Result = "KO"
		return fmt.Errorf("redis not running")
	}

	receiver := req.GetReceiver()
	list := client.SMembers(receiver)

	filename, _ := list.Result()
	file, _ := ioutil.ReadFile(filename[0])
	rsp.File = file
	client.SRem(receiver, filename)
	return nil
}

func main() {
	log.Println("running micrognui...")

	service := micro.NewService(
		micro.Name("ognui"),
		micro.RegisterTTL(time.Second*30),
		micro.RegisterInterval(time.Second*10),
	)
	service.Init()

	// Register Handlers
	proto.RegisterOgnuiHandler(service.Server(), new(Ognui))

	// Run server
	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
