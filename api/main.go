package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"mime"
	"mime/multipart"
	"strings"

	proto "bitbucket.org/blackdev1l/micrognui/proto"
	"github.com/micro/go-micro"
	"github.com/micro/go-micro/errors"
	api "github.com/micro/micro/api/proto"

	"golang.org/x/net/context"
)

type Ognui struct {
	Client proto.OgnuiClient
}

func (o *Ognui) Save(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Println("Received Ognui.Save API request")
	h := req.Header
	body := req.Body
	_, params, err := mime.ParseMediaType(strings.Join(h["Content-Type"].Values, ""))
	if err != nil {
		log.Println(err)
	}
	r := strings.NewReader(body)
	mr := multipart.NewReader(r, params["boundary"])
	form, err := mr.ReadForm(0)
	if err != nil {
		log.Println(err)
		return errors.BadRequest("go.micro.api.ognui", "request  error")
	}
	log.Println(form.Value["sender"])

	sender, ok := form.Value["sender"]

	if !ok || len(sender) == 0 {
		return errors.BadRequest("go.micro.api.ognui", "sender cannot be blank")
	}
	receiver, ok := form.Value["receiver"]
	if !ok || len(receiver) == 0 {
		return errors.BadRequest("go.micro.api.ognui", "receiver cannot be blank")
	}
	file := form.File["file"]
	filename := file[0].Filename
	f, _ := file[0].Open()
	defer f.Close()
	g, _ := ioutil.ReadAll(f)
	response, err := o.Client.Save(ctx, &proto.Request{
		Sender:   strings.Join(sender, " "),
		Receiver: strings.Join(receiver, " "),
		File:     g,
		Filename: filename,
	})
	if err != nil {
		return err
	}

	rsp.StatusCode = 200
	b, _ := json.Marshal(map[string]string{
		"message": response.Result,
	})
	rsp.Body = string(b)

	return nil
}

func (o *Ognui) List(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Println("Received Ognui.List API request")
	receiver := req.Get["receiver"]
	response, err := o.Client.List(ctx, &proto.Request{
		Sender:   "",
		Receiver: strings.Join(receiver.Values, ""),
		File:     []byte("nil"),
		Filename: "",
	})
	if err != nil {
		return err
	}

	rsp.StatusCode = 200
	b, _ := json.Marshal(map[string]string{
		"message": response.Result,
	})
	rsp.Body = string(b)

	return nil
}

func (o *Ognui) Retrieve(ctx context.Context, req *api.Request, rsp *api.Response) error {
	log.Println("Received Ognui.Retrieve API request")
	receiver := req.Get["receiver"]
	response, err := o.Client.Retrieve(ctx, &proto.Request{
		Sender:   "",
		Receiver: strings.Join(receiver.Values, ""),
		File:     []byte("nil"),
		Filename: "",
	})
	if err != nil {
		return err
	}

	rsp.StatusCode = 200

	rsp.Body = string(response.File)
	return nil
}

func main() {
	service := micro.NewService(
		micro.Name("go.micro.api.ognui"),
	)

	// parse command line flags
	service.Init()

	service.Server().Handle(
		service.Server().NewHandler(
			&Ognui{Client: proto.NewOgnuiClient("ognui", service.Client())},
		),
	)

	if err := service.Run(); err != nil {
		log.Fatal(err)
	}
}
