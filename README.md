# Micrognui

## install go 1.7 
`wget https://storage.googleapis.com/golang/go1.7.4.linux-amd64.tar.gz`  
`sudo tar -xvf go1.7.4.linux-amd64.tar.gz`  
`sudo mv go /opt/`  
## setup Go Environment
place the following lines in your .bashrc/.zshrc/whatever  
`export GOROOT=/opt/go`  
`export GOPATH=~/dev/go`  
`export PATH=$GOPATH/bin:$GOROOT/bin:$PATH`  


## Protobuf

get protobuf-go fork  
`go get github.com/micro/protobuf/{proto,protoc-gen-go}`  

get protobuf compiler  
`wget https://github.com/google/protobuf/releases/download/v3.2.0/protoc-3.2.0-linux-x86_64.zip`  

## Consul

get consul    
`sudo apt install consul`  

run consul  
`consul agent -dev`
